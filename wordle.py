import requests as rq
from typing import Self
import random

DEBUG = False

class WordleBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    create_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self: Self, name: str):
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WordleBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        create_dict = {"id": self.me, "overwrite": True}
        self.session.post(WordleBot.create_url, json=create_dict)

        self.choices = WordleBot.words[:]
        random.shuffle(self.choices)

    def play(self: Self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WordleBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']

        while not won:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(choice, feedback)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

<<<<<<< HEAD
    def update(self: Self, choice: str, feedback: str):
        def matches_feedback(word: str, choice: str, feedback: str) -> bool:
            # Create lists for green, yellow, and red positions
            green = [c == 'G' for c in feedback]
            yellow = [c == 'Y' for c in feedback]
            red = [c == 'R' for c in feedback]
=======
    def update(self: Self, choice: str, right: int):
        def common(choice: str, word: str):
            return len(set(choice) & set(wo def is_valid_choice(word: str, choice: str, feedback: str) -> bool:
            for i in range(len(word)):
                if feedback[i] == 'G' and word[i] != choice[i]:
                    return True
                if feedback[i] == 'Y' and (word[i] == choice[i] or choice[i] not in word):
                    return False
                if feedback[i] == 'R' and choice[i] in word:
                    return False
            return True

        self.choices = [w for w in self.choices if is_valid_choice(w, choice, feedback)]rd))
>>>>>>> a597cfb50ddbf590596e2704619707f0a77bc457

            # Check green positions
            for i, is_green in enumerate(green):
                if is_green and word[i] != choice[i]:
                    return False

<<<<<<< HEAD
            # Check yellow positions
            for i, is_yellow in enumerate(yellow):
                if is_yellow:
                    if word[i] == choice[i] or choice[i] not in word:
                        return False

            # Check red positions
i            for i, is_red in enumerate(red):
                if is_red and word[i] in choice:
                    return False

            # Ensure all yellow letters exist in the word, but in different positions
            for i, is_yellow in enumerate(yellow):
                if is_yellow and word.count(choice[i]) < 1:
                    return False

            return True

        self.choices = [w for w in self.choices if matches_feedback(w, choice, feedback)]

game = WordleBot("CodeShifu")
=======
<<<<<<< HEAD
      
=======
game = MMBot("Kavya")
>>>>>>> a597cfb50ddbf590596e2704619707f0a77bc457
game.play()
>>>>>>> 62ae433d14df720b86fd950b9da27577de24ccca

