import requests as rq
import random

DEBUG = False

class WordleBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    create_url = mm_url + "create"
    guess_url = mm_url + "guess"

    def __init__(self: Self, name: str):
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WordleBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        create_dict = {"id": self.me, "overwrite": True}
        self.session.post(WordleBot.create_url, json=create_dict)

        self.choices = WordleBot.words[:]
        random.shuffle(self.choices)

    def play(self: Self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WordleBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']

        while not won:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(choice, feedback)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, feedback: str):
        def matches(word: str, choice: str, feedback: str) -> bool:
            for i,char in enumerate(word):
                if feedback[i]=="G":
                    if word[i]!=choice[i]:
                        return False
                if feedback[i] == "Y":
                    if word[i] == choice[i] or choice[i] not in word:
                        return False

                if feedback[i]== " R":
                    if word[i] in choice:
                        return False

            return True

        self.choices = [w for w in self.choices if matches(w, choice, feedback)]

game = Wordl("Kavya")
game.play()

